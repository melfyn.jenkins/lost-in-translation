import { useContext, useEffect } from 'react';
import styles from './Header.module.css'
import { Link } from 'react-router-dom'
import { AppContext } from '../../utils/AppProvider'
import { getStorage } from '../../utils/storage'

function Header() {

    const [user, setUser] = useContext(AppContext);

    return (
        <header className={styles.Header}>
            <img className={styles.Image} src={require('../../assets/logo/Logo.png').default} alt='logo' />
            <h1 className={styles.Title}>Lost in Translation</h1>

            <p className={styles.AccountName}>{user}</p>
            <Link to='/profile'>
                <img className={styles.AccountIcon} src={require('../../assets/icon/account_circle.svg').default} alt='account icon'></img>
            </Link>
        </header >
    )
}


export default Header