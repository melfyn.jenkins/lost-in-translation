import { useState, useContext } from 'react'
import { checkUsername, createUsername } from './LoginAPI'
import { setStorage } from '../../utils/storage'
import styles from './Login.module.css'
import { Link } from 'react-router-dom'
import { AppContext } from '../../utils/AppProvider'
import { getStorage } from '../../utils/storage'

function Login() {

    const [username, setUsername] = useState('')

    const [user, setUser] = useContext(AppContext); // test code

    const onUsernameChange = event => {
        // update translate input
        setUsername(event.target.value)
    }

    // asynch because i might need to do several requests
    const onLoginClick = async () => {
        // check username or register a new one
        try {
            const foundUser = await checkUsername(username)
            if (foundUser) {
                // first store session
                setStorage('_lit-ss', username) // see under apllication local storage. key = lit, username is encoded
            } else {
                const createUser = await createUsername(username)
                console.log(createUser)
            }
            setStorage('_lit-ss', username)
            setUser(getStorage('_lit-ss')) // this is a test
        }
        catch (e) {
            console.log(e.message)
        }


    }
    // Consider combining headers later
    return (
        <>
            <header className={styles.Header}>
                <img className={styles.Image} src={require('../../assets/logo/Logo.png').default} alt='logo' />
                <h1 className={styles.Title}>Lost in Translation</h1>
            </header>
            <form>
                <input className={styles.Input} type="text" placeholder="Enter your username" onChange={onUsernameChange}></input>
                <Link to='/'>
                    <button className={styles.LoginButton} type="button" onClick={onLoginClick}>Login</button>
                </Link>
            </form>
        </>
    )

}

export default Login