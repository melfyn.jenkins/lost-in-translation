const BASE_URL = 'http://localhost:5000'

export function checkUsername(username) {
    return fetch(` ${BASE_URL}/users`)
        .then(response => response.json())  //parse to json
        .then(users => {
            // finding a user with username from input
            return users.find(user => user.username === username)
        })
}

// create user if it doesn't exist
export function createUsername(username) {
    return fetch(` ${BASE_URL}/users`, {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify({ username })
    })
        .then(response => response.json())  //parse to json

}