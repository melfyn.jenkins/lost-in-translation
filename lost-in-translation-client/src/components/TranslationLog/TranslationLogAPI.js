const BASE_URL = 'http://localhost:5000'

export function getUserTranslations() {
    return fetch(` ${BASE_URL}/userTranslations`)
        .then(response => response.json())
        .then(userTranslations => {
            return userTranslations;
        });
}