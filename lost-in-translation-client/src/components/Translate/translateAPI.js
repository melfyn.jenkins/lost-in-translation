const BASE_URL = 'http://localhost:5000'

// fecthes all users and translations / temporary to avoid overwriting data
export function getTranslations() {
    return fetch(` ${BASE_URL}/userTranslations`)
        .then(response => response.json())
        .then(translations => {
            return translations;
        });
}

export function storeWord(id, word) {
    // Get request for all translations and processes the request in .then
    getTranslations().then(everyonesTranslations => {

        console.log(everyonesTranslations);
        // Check if user exists in the translate object
        if (everyonesTranslations[id] == null) {
            // If it doesn't exist, create new object with user name and an empty array which the translatations can be pushed to
            everyonesTranslations[id] = [];
        }

        // Translate object
        const myNewTranslation = {
            word: word,
            active: true
        };

        // pushes the user translation to the users name
        everyonesTranslations[id].push(myNewTranslation);

        return fetch(` ${BASE_URL}/userTranslations`, {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(everyonesTranslations)
        })
            .then(response => response.json());

    });
}
