import { useState, useContext } from 'react'
import styles from './Translate.module.css'
import Handsign from '../Handsign/Handsign'
import Header from '../Header/Header'
import { storeWord } from './translateAPI'
import { getStorage } from '../../utils/storage'

function Translate() {

    const [translateInput, setTranslateInput] = useState('')
    const [letters, setLetters] = useState([])
    const [invalidInput, setInvalidInput] = useState('')

    const onTranslateInputChange = event => {
        // update translate input
        setTranslateInput(event.target.value)
    }

    const onTranslateClick = () => {
        // check for input: allowed characters a-zA-Z and spaces
        console.log(translateInput)
        if (translateInput.match(/^[A-Za-z ]+$/)) {
            const inputLowerCase = translateInput.toLowerCase() // toLowerCase to match .png filename
            const inputReplaceSpace = inputLowerCase.replaceAll(' ', '_') // hacky work round for spaces when iterating letters = empty png name _.png
            const inputSplit = inputReplaceSpace.split('')
            // Might want to revisit this code and see if i can come up with a better solution
            setLetters(inputSplit)
            setInvalidInput('')
            // should be rewored
            const username = getStorage('_lit-ss')
            storeWord(username, inputReplaceSpace);
        } else {
            setInvalidInput('Invalid input: Only the english alphabet and spaces allowed')
            setLetters([])
        }
    }

    return (
        <section >
            <Header />
            <input
                className={styles.Input}
                type="text"
                placeholder="Write a word or sentence"
                value={translateInput} onChange={onTranslateInputChange}></input>
            <button
                className={styles.TranslateButton}
                onClick={onTranslateClick}>Translate </button>
            {
                // listing handsigns from handsign component
            }
            <div className={styles.TranslateResult}>
                <p>{invalidInput}</p>
                {
                    letters.map((letter, index) => <Handsign letter={letter} key={index} />)
                }
            </div>
        </section>
    )
}

export default Translate
