import { Link } from 'react-router-dom'
import styles from './Profile.module.css'
import TranslationLog from '../TranslationLog/TranslationLog'
import { getUserTranslations } from '../TranslationLog/TranslationLogAPI'

function Profile() {

    // clear local storage when logging out 
    function onLogoutClick() {
        localStorage.clear();
        console.log('logout click')
    }

    const onClearClick = () => {

        const getTranslations = getUserTranslations()

        console.log(getTranslations)
    }

    return (
        <>
            <header className={styles.Header}>
                <img className={styles.Image} src={require('../../assets/logo/Logo.png').default} alt='logo' />
                <Link className={styles.Link} to='/'>
                    <h1 className={styles.Title}>Lost in Translation</h1>
                </Link>
                <Link className={styles.Link} to='/login' onClick={onLogoutClick}>
                    <p>Log out</p>
                </Link>
            </header >

            <TranslationLog />

            <button className={styles.ClearLogButton} onClick={onClearClick}>Clear log</button>
        </>
    )
}

export default Profile