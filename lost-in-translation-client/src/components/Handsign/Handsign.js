import styles from './Handsign.module.css'

function Handsign({ letter, index }) {
    // assigns src and alt with letter input
    return (
        <div className={styles.HandsignContainer}>
            <img className={styles.Handsign} src={require(`../../assets/signs/${letter}.png`).default} alt={letter} key={index} />
        </div>
    )
}

export default Handsign