import { Redirect, Route } from 'react-router-dom'
import Translate from '../Translate/Translate'

function ProtectedRoute() {

    const isLoggedIn = localStorage.getItem('_lit-ss')

    return isLoggedIn ? (
        <Translate />

    ) : (
            <Redirect to='/login'></Redirect>
        );
}

export default ProtectedRoute