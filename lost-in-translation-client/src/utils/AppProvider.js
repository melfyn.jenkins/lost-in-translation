import { createContext, useState } from 'react'


export const AppContext = createContext()

export function AppProvider(props) {    // needs to have props

    const [user, setUser] = useState('')

    return (

        <AppContext.Provider value={[user, setUser]}>
            {props.children}
        </AppContext.Provider>

    )
}

// .Provider = provide state to the app via AppContext