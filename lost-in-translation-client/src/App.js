import './App.css';
import Translate from './components/Translate/Translate'
import Login from './components/Login/Login'
import Profile from './components/Profile/Profile'
import ProtectedRoute from './components/ProtectedRoute/ProtectedRoute'
import { getStorage } from './utils/storage'
import { createContext, useEffect, useContext } from 'react'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'; // as renames BR to router
import { AppContext } from './utils/AppProvider'

function App() {

  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path='/' exact component={Translate} />
          <Route path='/login' component={Login} />
          <Route path='/profile' component={Profile} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
