const jsonServer = require('json-server')
const server = jsonServer.create()
const router = jsonServer.router('db.json')
const middlewares = jsonServer.defaults()
const { PORT = 5000 } = process.env
server.use(middlewares)
server.use(router)

//var handsigns = JSON.parse(fs.readFileSync('handsigns.json', 'utf-8'));


//app.use('/handsigns', express.static('handsigns'));

/*
app.get('/handsigns', (request, response) => {
    return response.send(handsigns);
}); */
/*
app.get('/translate/:word', (req, response) => {
    let signImage = [];
    let word = req.params.word;

    console.log(handsigns);

    word.split("").forEach(letter => {
        signImage.push(handsigns[letter]);
    });

    return response.send(JSON.stringify(signImage));
}); */
/*
app.get('/', (request, response) => {
    return response.send('Helluuu from the dark side');

}); */


/*
app.get('/asset', (req, response) => {
    console.log(req.params);
    return response.send('potet');
});
*/
server.listen(PORT, () => console.log(`Server started on port ${PORT}`));
